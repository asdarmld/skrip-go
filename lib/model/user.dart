part of 'models.dart';

class UserModel extends Equatable {
  final String id;
  final String email;
  final String name;
  final String hp;
  static String token;

  UserModel({this.id, this.email, this.name, this.hp});

  factory UserModel.fromJson(Map<String, dynamic> data) => UserModel(
        id: data['id'].toString(),
        name: data['name'],
        email: data['email'],
        hp: data['hp'],
      );

  @override
  List<Object> get props => [id, email, name, hp];
}
