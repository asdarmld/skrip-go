part of 'models.dart';

class RegistrationData {
  String name;
  String email;
  String hp;
  String password;

  RegistrationData(
      {this.name = "", this.email = "", this.hp = "", this.password = ""});
}
