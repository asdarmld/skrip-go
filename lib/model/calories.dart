part of 'models.dart';

class CaloriesModel {
  final String id;
  final String kategori;
  final String nama;
  final int kalori;
  final String gambar;

  CaloriesModel({this.id, this.kategori, this.nama, this.kalori, this.gambar});

  factory CaloriesModel.fromJson(Map<String, dynamic> json) => CaloriesModel(
        id: json["id"].toString(),
        nama: json["nama"],
        kalori: json["kalori"],
        gambar: json["gambar"],
        kategori: json["kategori"],
      );
}
