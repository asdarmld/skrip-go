import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';

part 'user.dart';
part 'api_return_value.dart';
part 'family.dart';
part 'registration_data.dart';
part 'total.dart';
part 'calories.dart';
part 'rekomendasi.dart';
part 'total_riwayat.dart';
