part of 'models.dart';

class FamilyModel {
  final String id;
  final String userId;
  final String nama;
  final String usia;
  final String tinggi;
  final String beratBadan;
  final String gender;
  final String faktor;
  //
  final String protein;
  final String karbohidrat;
  final String lemak;
  final String energi;

  FamilyModel(
      {this.id,
      this.userId,
      this.nama,
      this.usia,
      this.tinggi,
      this.beratBadan,
      this.gender,
      this.faktor,
      this.protein,
      this.karbohidrat,
      this.lemak,
      this.energi});

  factory FamilyModel.fromJson(Map<String, dynamic> json) => FamilyModel(
        id: json["id"].toString(),
        userId: json["userId"],
        nama: json["nama"],
        usia: json["usia"],
        tinggi: json["tinggi"],
        beratBadan: json["beratBadan"],
        gender: json["gender"],
        faktor: json["faktor"],
        karbohidrat: json["karbohidrat"],
        protein: json["protein"],
        lemak: json["lemak"],
        energi: json["energi"],
      );
}
