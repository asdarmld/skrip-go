part of 'models.dart';

class TotalRiwayatModel {
  String id;
  String protein;
  String lemak;
  String karbohidrat;
  String energi;
  String tgl;

  TotalRiwayatModel(
      {this.id,
      this.karbohidrat,
      this.lemak,
      this.protein,
      this.energi,
      this.tgl});

  factory TotalRiwayatModel.fromJson(Map<String, dynamic> json) =>
      TotalRiwayatModel(
        id: json["id"].toString(),
        karbohidrat: json["karbohidrat"].toString(),
        protein: json["protein"].toString(),
        lemak: json["lemak"].toString(),
        energi: json["energi"].toString(),
        tgl: convertDate(json['created_at']),
      );
}

final DateFormat formatter = DateFormat('dd-MM-yyyy');

convertDate(tgl) {
  if (tgl != null) {
    DateTime todayDate = DateTime.parse(tgl);
    return formatter.format(todayDate).toString();
  }
  return '-';
}
