part of 'models.dart';

class RekomendasiModel {
  final String id;
  final String kategori;
  final String nama;
  final int kalori;
  final String gambar;
  static List statis;

  RekomendasiModel(
      {this.id, this.kategori, this.nama, this.kalori, this.gambar});

  factory RekomendasiModel.fromJson(Map<String, dynamic> json) =>
      RekomendasiModel(
        id: json["id"].toString(),
        nama: json["nama"],
        kalori: json["kalori"],
        gambar: json["gambar"],
        kategori: json["kategori"],
      );
}
