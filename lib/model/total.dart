part of 'models.dart';

class TotalFamily {
  String protein;
  String lemak;
  String karbohidrat;
  String energi;

  TotalFamily({this.karbohidrat, this.lemak, this.protein, this.energi});

  factory TotalFamily.fromJson(Map<String, dynamic> json) => TotalFamily(
        karbohidrat: json["karbohidrat"].toString(),
        protein: json["protein"].toString(),
        lemak: json["lemak"].toString(),
        energi: json["energi"].toString(),
      );
}
