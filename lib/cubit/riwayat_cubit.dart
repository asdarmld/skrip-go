import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:skrip_go/model/models.dart';
import 'package:skrip_go/services/services.dart';

part 'riwayat_state.dart';

class RiwayatCubit extends Cubit<RiwayatState> {
  RiwayatCubit() : super(RiwayatInitial());
  Future<void> getRiwayat() async {
    ApiReturnValue<List<TotalRiwayatModel>> result =
        await RiwayatServices.getRiwayat();
    if (result.value != null) {
      emit(RiwayatLoaded(result.value));
    } else {
      emit(RiwayatLoadingFailed(result.message));
    }
  }
}
