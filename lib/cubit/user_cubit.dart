import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:skrip_go/model/models.dart';
import 'package:skrip_go/services/services.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  UserCubit() : super(UserInitial());

  Future<void> signIn(String email, String password) async {
    ApiReturnValue<UserModel> result =
        await UserServices.signIn(email, password);

    if (result.value != null) {
      emit(UserLoaded(result.value));
    } else {
      emit(UserLoadingFailed(result.message));
    }
  }

  Future<void> getUser(String accessToken) async {
    ApiReturnValue<UserModel> result = await UserServices.getUser(accessToken);

    if (result.value != null) {
      emit(UserLoaded(result.value));
    } else {
      emit(UserLoadingFailed(result.message));
    }
  }

  Future<void> register(
      String name, String email, String password, String hp) async {
    ApiReturnValue<UserModel> result =
        await UserServices.register(email, password, name, hp);

    if (result.value != null) {
      emit(UserLoaded(result.value));
    } else {
      emit(UserLoadingFailed(result.message));
    }
  }
}
