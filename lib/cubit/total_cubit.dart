import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:skrip_go/model/models.dart';
import 'package:skrip_go/services/services.dart';

part 'total_state.dart';

class TotalCubit extends Cubit<TotalState> {
  TotalCubit() : super(TotalInitial());

  Future<void> getTotal() async {
    ApiReturnValue<TotalFamily> result = await FamilyService.getTotal();
    if (result.value != null) {
      emit(FamilyTotalLoaded(result.value));
    } else {
      return null;
    }
  }
}
