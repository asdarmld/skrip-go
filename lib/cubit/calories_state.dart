part of 'calories_cubit.dart';

abstract class CaloriesState extends Equatable {
  const CaloriesState();

  @override
  List<Object> get props => [];
}

class CaloriesInitial extends CaloriesState {}

class CaloriesLoaded extends CaloriesState {
  final List<CaloriesModel> calories;
  CaloriesLoaded(this.calories);

  List<Object> get props => [calories];
}

class CaloriesLoadingFailed extends CaloriesState {
  final message;
  CaloriesLoadingFailed(this.message);

  List<Object> get props => [message];
}
