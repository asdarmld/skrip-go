part of 'detail_riwayat_cubit.dart';

abstract class DetailRiwayatState extends Equatable {
  const DetailRiwayatState();

  @override
  List<Object> get props => [];
}

class DetailRiwayatInitial extends DetailRiwayatState {}

class DetailRiwayatLoaded extends DetailRiwayatState {
  final List<RekomendasiModel> rekomendasi;
  DetailRiwayatLoaded(this.rekomendasi);

  List<Object> get props => [rekomendasi];
}

class DetailRiwayatLoadingFailed extends DetailRiwayatState {
  final message;
  DetailRiwayatLoadingFailed(this.message);

  List<Object> get props => [message];
}
