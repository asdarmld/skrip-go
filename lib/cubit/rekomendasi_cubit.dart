import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:skrip_go/model/models.dart';
import 'package:skrip_go/services/services.dart';

part 'rekomendasi_state.dart';

class RekomendasiCubit extends Cubit<RekomendasiState> {
  RekomendasiCubit() : super(RekomendasiInitial());

  Future<void> getRekomendasi() async {
    ApiReturnValue<List<RekomendasiModel>> result =
        await CaloriesServices.getRekomendasi();
    if (result.value != null) {
      emit(RekomendasiLoaded(result.value));
    } else {
      emit(RekomendasiLoadingFailed(result.message));
    }
  }
}
