import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:skrip_go/model/models.dart';
import 'package:skrip_go/services/services.dart';

part 'calories_state.dart';

class CaloriesCubit extends Cubit<CaloriesState> {
  CaloriesCubit() : super(CaloriesInitial());

  Future<void> getCalories() async {
    ApiReturnValue<List<CaloriesModel>> result =
        await CaloriesServices.getCalories();
    if (result.value != null) {
      emit(CaloriesLoaded(result.value));
    } else {
      emit(CaloriesLoadingFailed(result.message));
    }
  }
}
