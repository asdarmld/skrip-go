import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:skrip_go/model/models.dart';
import 'package:skrip_go/services/services.dart';

part 'detail_riwayat_state.dart';

class DetailRiwayatCubit extends Cubit<DetailRiwayatState> {
  DetailRiwayatCubit() : super(DetailRiwayatInitial());
  Future<void> getDetailRiwayat(String id) async {
    ApiReturnValue<List<RekomendasiModel>> result =
        await RiwayatServices.getDetailRiwayat(id);
    if (result.value != null) {
      emit(DetailRiwayatLoaded(result.value));
    } else {
      emit(DetailRiwayatLoadingFailed(result.message));
    }
  }
}
