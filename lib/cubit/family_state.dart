part of 'family_cubit.dart';

abstract class FamilyState extends Equatable {
  const FamilyState();

  @override
  List<Object> get props => [];
}

class FamilyInitial extends FamilyState {}

class FamilyLoaded extends FamilyState {
  final List<FamilyModel> family;
  FamilyLoaded(this.family);

  List<Object> get props => [family];
}

class FamilyLoadingFailed extends FamilyState {
  final message;
  FamilyLoadingFailed(this.message);

  List<Object> get props => [message];
}
