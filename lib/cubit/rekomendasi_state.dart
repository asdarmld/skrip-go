part of 'rekomendasi_cubit.dart';

abstract class RekomendasiState extends Equatable {
  const RekomendasiState();

  @override
  List<Object> get props => [];
}

class RekomendasiInitial extends RekomendasiState {}

class RekomendasiLoaded extends RekomendasiState {
  final List<RekomendasiModel> rekomendasi;
  RekomendasiLoaded(this.rekomendasi);

  List<Object> get props => [rekomendasi];
}

class RekomendasiLoadingFailed extends RekomendasiState {
  final message;
  RekomendasiLoadingFailed(this.message);

  List<Object> get props => [message];
}
