part of 'riwayat_cubit.dart';

abstract class RiwayatState extends Equatable {
  const RiwayatState();

  @override
  List<Object> get props => [];
}

class RiwayatInitial extends RiwayatState {}

class RiwayatLoaded extends RiwayatState {
  final List<TotalRiwayatModel> riwayat;
  RiwayatLoaded(this.riwayat);

  List<Object> get props => [riwayat];
}

class RiwayatLoadingFailed extends RiwayatState {
  final message;
  RiwayatLoadingFailed(this.message);

  List<Object> get props => [message];
}
