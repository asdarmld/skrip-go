part of 'jumlah_kalori_cubit.dart';

abstract class JumlahKaloriState extends Equatable {
  const JumlahKaloriState();

  @override
  List<Object> get props => [];
}

class JumlahKaloriInitial extends JumlahKaloriState {}

class JumlahKaloriLoaded extends JumlahKaloriState {
  final message;
  JumlahKaloriLoaded(this.message);

  List<Object> get props => [message];
}
