part of 'total_cubit.dart';

abstract class TotalState extends Equatable {
  const TotalState();

  @override
  List<Object> get props => [];
}

class TotalInitial extends TotalState {}

class FamilyTotalLoaded extends TotalState {
  final TotalFamily totalFamily;
  FamilyTotalLoaded(this.totalFamily);
  @override
  List<Object> get props => [totalFamily];
}
