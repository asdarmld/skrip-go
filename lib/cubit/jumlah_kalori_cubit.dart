import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:skrip_go/shared/shared.dart';

part 'jumlah_kalori_state.dart';

class JumlahKaloriCubit extends Cubit<JumlahKaloriState> {
  JumlahKaloriCubit() : super(JumlahKaloriInitial());

  Future<void> getJumlahKalori() async {
    var data = cekJumlahKalori();
    print(data);
    if (data) {
      emit(JumlahKaloriLoaded(data));
    } else {
      return null;
    }
  }
}
