import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:skrip_go/model/models.dart';
import 'package:skrip_go/services/services.dart';

part 'family_state.dart';

class FamilyCubit extends Cubit<FamilyState> {
  FamilyCubit() : super(FamilyInitial());

  Future<void> getFamily() async {
    ApiReturnValue<List<FamilyModel>> result = await FamilyService.getFamily();
    if (result.value != null) {
      emit(FamilyLoaded(result.value));
    } else {
      emit(FamilyLoadingFailed(result.message));
    }
  }

  Future<void> submitFamily(
      String nama, String tinggi, usia, beratBadan, gender, faktor) async {
    ApiReturnValue<List<FamilyModel>> result = await FamilyService.submitFamily(
        nama, usia, tinggi, beratBadan, gender, faktor);

    if (result.value != null) {
      emit(FamilyLoaded(result.value));
    } else {
      emit(FamilyLoadingFailed(result.message));
    }
  }

  Future<void> deleteFamily(String id) async {
    ApiReturnValue<List<FamilyModel>> result =
        await FamilyService.deleteFamily(id);
    if (result.value != null) {
      emit(FamilyLoaded(result.value));
    } else {
      emit(FamilyLoadingFailed(result.message));
    }
  }
}
