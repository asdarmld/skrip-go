import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import '.././model/models.dart';
import 'package:http/http.dart' as http;
import '.././shared/shared.dart';

part 'user_service.dart';
part 'family_services.dart';
part 'calories_services.dart';
part 'riwayat_services.dart';

String baseURL = 'https://skrip-go.maccatechno.com/api/';

String assetsURL = 'https://skrip-go.maccatechno.com/assets/';
