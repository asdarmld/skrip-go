part of 'services.dart';

class UserServices {
  static Future<ApiReturnValue<UserModel>> signIn(String email, String password,
      {http.Client client}) async {
    if (client == null) {
      client = http.Client();
    }

    String url = baseURL + 'login';

    var response = await client.post(Uri.parse(url),
        headers: {"Content-Type": "application/json"},
        body:
            jsonEncode(<String, String>{'email': email, 'password': password}));

    if (response.statusCode != 200) {
      print(1);
      return ApiReturnValue(message: 'Please try again');
    }
    var data = jsonDecode(response.body);

    UserModel.token = data['data']['access_token'];
    UserModel value = UserModel.fromJson(data['data']['user']);
    print(data);
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("access_token", data['data']['access_token']);

    return ApiReturnValue(value: value);
  }

  static Future<ApiReturnValue<UserModel>> register(
      String email, String password, String name, String hp,
      {http.Client client}) async {
    if (client == null) {
      client = http.Client();
    }

    String url = baseURL + 'register';

    var response = await client.post(Uri.parse(url),
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(<String, String>{
          'email': email,
          'password': password,
          'name': name,
          'hp': hp
        }));

    var data = jsonDecode(response.body);

    UserModel.token = data['data']['access_token'];
    UserModel value = UserModel.fromJson(data['data']['user']);
    print(data);
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("access_token", data['data']['access_token']);

    return ApiReturnValue(value: value);
  }

  static Future<ApiReturnValue<UserModel>> getUser(String token,
      {http.Client client}) async {
    client ??= http.Client();

    String url = baseURL + 'user';

    var response = await client.get(Uri.parse(url), headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer $token}"
    });

    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'Please try again');
    }

    var data = jsonDecode(response.body);

    SharedPreferences preferences = await SharedPreferences.getInstance();
    UserModel.token = preferences.getString("access_token");
    UserModel value = UserModel.fromJson(data['data']);

    return ApiReturnValue(value: value);
  }
}
