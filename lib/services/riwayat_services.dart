part of 'services.dart';

class RiwayatServices {
  static Future<ApiReturnValue<List<TotalRiwayatModel>>> getRiwayat(
      {http.Client client}) async {
    client ??= http.Client();

    String url = baseURL + 'riwayat';

    var response = await client.get(Uri.parse(url), headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${UserModel.token}",
    });

    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'Please try again');
    }

    var data = jsonDecode(response.body);

    List<TotalRiwayatModel> riwayat = (data['data'] as Iterable)
        .map((e) => TotalRiwayatModel.fromJson(e))
        .toList();

    return ApiReturnValue(value: riwayat);
  }

  static Future<ApiReturnValue<List<RekomendasiModel>>> getDetailRiwayat(
      String id,
      {http.Client client}) async {
    client ??= http.Client();

    String url = baseURL + "riwayat/$id";

    var response = await client.get(Uri.parse(url), headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${UserModel.token}",
    });

    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'Please try again');
    }

    var data = jsonDecode(response.body);
    List<RekomendasiModel> family = (data['data']['list'] as Iterable)
        .map((e) => RekomendasiModel.fromJson(e))
        .toList();

    return ApiReturnValue(value: family);
  }
}
