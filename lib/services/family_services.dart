part of 'services.dart';

class FamilyService {
  static Future<ApiReturnValue<List<FamilyModel>>> getFamily(
      {http.Client client}) async {
    client ??= http.Client();

    String url = baseURL + 'family';

    var response = await client.get(Uri.parse(url), headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${UserModel.token}",
    });

    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'Please try again');
    }

    var data = jsonDecode(response.body);

    List<FamilyModel> family =
        (data['data'] as Iterable).map((e) => FamilyModel.fromJson(e)).toList();

    return ApiReturnValue(value: family);
  }

  static Future<ApiReturnValue<List<FamilyModel>>> submitFamily(
      String nama,
      String usia,
      String tinggi,
      String beratBadan,
      String gender,
      String faktor,
      {http.Client client}) async {
    client ??= http.Client();

    String url = baseURL + 'family';

    var response = await client.post(url,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${UserModel.token}",
        },
        body: jsonEncode(<String, dynamic>{
          'nama': nama,
          'tinggi': tinggi,
          'usia': usia,
          'beratBadan': beratBadan,
          'gender': gender,
          'faktor': faktor,
        }));

    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'Please try again');
    }

    var data = jsonDecode(response.body);

    List<FamilyModel> family =
        (data['data'] as Iterable).map((e) => FamilyModel.fromJson(e)).toList();

    return ApiReturnValue(value: family);
  }

  static Future<ApiReturnValue<List<FamilyModel>>> deleteFamily(String id,
      {http.Client client}) async {
    client ??= http.Client();

    String url = baseURL + 'deletefamily';

    var response = await client.post(url,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${UserModel.token}",
        },
        body: jsonEncode(<String, dynamic>{
          'id': id,
        }));

    var data = jsonDecode(response.body);
    print(data);
    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'Please try again');
    }
    List<FamilyModel> family = (data['data']['list'] as Iterable)
        .map((e) => FamilyModel.fromJson(e))
        .toList();

    return ApiReturnValue(value: family);
  }

  static Future<ApiReturnValue<TotalFamily>> getTotal(
      {http.Client client}) async {
    client ??= http.Client();

    String url = baseURL + 'totalfamily';

    var response = await client.get(Uri.parse(url), headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${UserModel.token}",
    });

    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'Please try again');
    }

    var data = jsonDecode(response.body);

    TotalFamily total = TotalFamily.fromJson(data['data']);

    return ApiReturnValue(value: total);
  }
}
