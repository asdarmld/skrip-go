part of 'services.dart';

class CaloriesServices {
  static Future<ApiReturnValue<List<CaloriesModel>>> getCalories(
      {http.Client client}) async {
    client ??= http.Client();

    String url = baseURL + 'calories';

    var response = await client.get(Uri.parse(url), headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${UserModel.token}",
    });

    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'Please try again');
    }

    var data = jsonDecode(response.body);

    List<CaloriesModel> family = (data['data'] as Iterable)
        .map((e) => CaloriesModel.fromJson(e))
        .toList();

    return ApiReturnValue(value: family);
  }

  static Future<ApiReturnValue<List<RekomendasiModel>>> getRekomendasi(
      {http.Client client}) async {
    client ??= http.Client();

    String url = baseURL + 'calories/create';

    var response = await client.get(Uri.parse(url), headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${UserModel.token}",
    });

    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'Please try again');
    }

    var data = jsonDecode(response.body);
    RekomendasiModel.statis = data['data']['statis'];
    List<RekomendasiModel> family = (data['data']['list'] as Iterable)
        .map((e) => RekomendasiModel.fromJson(e))
        .toList();

    return ApiReturnValue(value: family);
  }

  static Future<ApiReturnValue> simpanRekomendasi({http.Client client}) async {
    if (client == null) {
      client = http.Client();
    }

    String url = baseURL + 'calories';

    var response = await client.post(Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${UserModel.token}",
        },
        body: jsonEncode(
            <String, String>{'hasil': RekomendasiModel.statis.toString()}));

    // var data = jsonDecode(response.body);
    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'Please try again');
    }

    return ApiReturnValue(message: 'Berhasil disimpan');
  }

  static Future<ApiReturnValue> simpanPilihan({http.Client client}) async {
    if (client == null) {
      client = http.Client();
    }

    String url = baseURL + 'calories';

    var response = await client.post(Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${UserModel.token}",
        },
        body: jsonEncode(<String, String>{'hasil': pilihan.toString()}));

    // var data = jsonDecode(response.body);
    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'Please try again');
    }

    return ApiReturnValue(message: 'Berhasil disimpan');
  }
}
