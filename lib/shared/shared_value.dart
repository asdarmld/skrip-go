part of 'shared.dart';

Widget loadingIndicator = SpinKitFadingCircle(
  size: 45,
  color: Colors.grey,
);

List<String> pilihan = [];

List<int> jumlahKalori = [];

num sum = 0;

cekJumlahKalori() {
  sum = 0;
  for (num e in jumlahKalori) {
    sum += e;
  }
  // return sum;
}

notifGagal(context, message) {
  return Flushbar(
    duration: Duration(seconds: 4),
    flushbarPosition: FlushbarPosition.TOP,
    backgroundColor: Colors.red,
    message: message,
  )..show(context);
}

notifBerhasil(context, message) {
  return Flushbar(
    duration: Duration(seconds: 4),
    flushbarPosition: FlushbarPosition.TOP,
    backgroundColor: Colors.green[600],
    message: message,
  )..show(context);
}

showAlertDialog(BuildContext context) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => SignInPage()),
      );
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Berhasil"),
    content: Text('Berhasil Register'),
    // backgroundColor: Colors.red[200],
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
