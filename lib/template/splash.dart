import 'dart:async';
import 'package:flutter/material.dart';
import 'package:skrip_go/template/signin.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    startSplashScreen();
  }

  startSplashScreen() async {
    var duration = const Duration(seconds: 5);
    return Timer(duration, () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => SignInPage()),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    var boxDecoration = BoxDecoration(
      // Box decoration takes a gradient
      gradient: LinearGradient(
        // Where the linear gradient begins and ends
        begin: Alignment.topRight,
        end: Alignment.bottomLeft,
        // Add one stop for each color. Stops should increase from 0 to 1
        stops: [0.1, 2],
        colors: [
          Color(0xff1a237e),
          Color(0xff1a237e),
        ],
      ),
    );
    return Scaffold(
      body: Container(
        decoration: boxDecoration,
        child: Center(
            child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/img/logosehat.png"))),
              ),
              SizedBox(
                height: 30.0,
              ),
              Text('SMART GROCERY',
                  style: TextStyle(
                      fontSize: 20, color: Colors.white, letterSpacing: 3)),
              SizedBox(height: 5),
              Text('App For Family Safe and Healthy',
                  style: TextStyle(color: Colors.white)),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: Text("Version 1.1"),
                  ))
            ],
          ),
        )),
      ),
    );
  }
}
