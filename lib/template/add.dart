import 'package:flutter/material.dart';

class Add extends StatefulWidget {
  @override
  _AddState createState() => _AddState();
}

class _AddState extends State<Add> {
  SingingCharacter _character = SingingCharacter.laki;
  FaktorAktifitas _characterFaktor = FaktorAktifitas.tidakaktif;

  // int selectedGander;
  // int selectedActivity;

  // @override
  // void initState (){
  //   super.initState();
  //   selectedGander = 0;
  //   selectedActivity = 0;
  // }

  // setselectedGander(int val){
  //   setState(() {
  //     selectedGander = val;
  //   });
  // }
  // setselectedActivity(int act){
  //   setState(() {
  //     selectedActivity = act;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          '',
          style: TextStyle(color: Colors.black),
        ),
        leading: BackButton(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: new LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints:
                  BoxConstraints(minHeight: viewportConstraints.maxHeight),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextField(
                        decoration: InputDecoration(
                          labelText: 'Nama',
                          labelStyle: TextStyle(fontSize: 15),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ),
                      TextField(
                        decoration: InputDecoration(
                          labelText: 'Usia',
                          labelStyle: TextStyle(fontSize: 15),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      TextField(
                        decoration: InputDecoration(
                          labelText: 'Tinggi',
                          labelStyle: TextStyle(fontSize: 15),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      TextField(
                        decoration: InputDecoration(
                          labelText: 'Berat Badan',
                          labelStyle: TextStyle(fontSize: 15),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 30),
                      Text('Jenis kelamin',
                          style: TextStyle(fontWeight: FontWeight.w600)),
                      SizedBox(height: 10),
                      ListTile(
                        title: const Text('Laki-laki'),
                        leading: Radio<SingingCharacter>(
                          value: SingingCharacter.laki,
                          groupValue: _character,
                          onChanged: (SingingCharacter value) {
                            setState(() {
                              _character = value;
                            });
                          },
                        ),
                      ),
                      ListTile(
                        title: const Text('Perempuan'),
                        leading: Radio<SingingCharacter>(
                          value: SingingCharacter.perempuan,
                          groupValue: _character,
                          onChanged: (SingingCharacter value) {
                            setState(() {
                              _character = value;
                            });
                          },
                        ),
                      ),
                      SizedBox(height: 30),
                      Text('Faktor',
                          style: TextStyle(fontWeight: FontWeight.w600)),
                      ListTile(
                        title: const Text('Tidak aktif'),
                        leading: Radio<FaktorAktifitas>(
                          value: FaktorAktifitas.tidakaktif,
                          groupValue: _characterFaktor,
                          onChanged: (FaktorAktifitas value) {
                            setState(() {
                              _characterFaktor = value;
                            });
                          },
                        ),
                      ),
                      ListTile(
                        title: const Text('Cukup aktif'),
                        leading: Radio<FaktorAktifitas>(
                          value: FaktorAktifitas.cukupaktif,
                          groupValue: _characterFaktor,
                          onChanged: (FaktorAktifitas value) {
                            setState(() {
                              _characterFaktor = value;
                            });
                          },
                        ),
                      ),
                      ListTile(
                        title: const Text('Aktif'),
                        leading: Radio<FaktorAktifitas>(
                          value: FaktorAktifitas.aktif,
                          groupValue: _characterFaktor,
                          onChanged: (FaktorAktifitas value) {
                            setState(() {
                              _characterFaktor = value;
                            });
                          },
                        ),
                      ),
                      ListTile(
                        title: const Text('Sangat aktif'),
                        leading: Radio<FaktorAktifitas>(
                          value: FaktorAktifitas.sangataktif,
                          groupValue: _characterFaktor,
                          onChanged: (FaktorAktifitas value) {
                            setState(() {
                              _characterFaktor = value;
                            });
                          },
                        ),
                      ),
                      SizedBox(height: 60),
                    ]),
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {},
        label: Text('Simpan'),
        icon: Icon(
          Icons.bookmark,
          color: Colors.white,
        ),
        backgroundColor: Color(0xff1a237e),
      ),
    );
  }
}

enum SingingCharacter { laki, perempuan }
enum FaktorAktifitas { tidakaktif, cukupaktif, aktif, sangataktif }
