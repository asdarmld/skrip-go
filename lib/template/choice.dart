import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';

class Choice extends StatefulWidget {
  @override
  _ChoiceState createState() => _ChoiceState();
}

class _ChoiceState extends State<Choice> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          '',
          style: TextStyle(color: Colors.black),
        ),
        leading: BackButton(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: new LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints:
                  BoxConstraints(minHeight: viewportConstraints.maxHeight),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                              child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.pink,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.pink),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Kebutuhan Kalori',
                                          style: TextStyle(
                                              color: Colors.pink,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text('123',
                                            style: TextStyle(
                                                color: Colors.pink,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                        ],
                      ),
                      SizedBox(height: 15),
                      Row(
                        children: [
                          Expanded(
                              child: Container(
                            // padding: EdgeInsets.all(10),

                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.green),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Karbohidrat',
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text('123',
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                          SizedBox(width: 15),
                          Expanded(
                              child: Container(
                            // padding: EdgeInsets.all(10),

                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.blue),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Protein',
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text('123',
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                          SizedBox(width: 15),
                          Expanded(
                              child: Container(
                            // padding: EdgeInsets.all(10),

                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.amber),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Lemak',
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text('123',
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      RichText(
                        text: TextSpan(
                          text:
                              'Silahkan memilih makanan sesuai dengan hasil perhitungan total kebutuhan kalori keluarga anda ',
                          style: TextStyle(color: Colors.black54, height: 1.5),
                          children: <TextSpan>[
                            TextSpan(
                              text:
                                  ', makanan yang dipilih tidak boleh melebihi kelori total',
                              style: TextStyle(color: Colors.black54),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      GridView.count(
                        primary: false,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        crossAxisCount: 2,
                        shrinkWrap: true,
                        children: <Widget>[
                          Card(),
                          Card(),
                          Card(),
                          Card(),
                          Card(),
                          Card(),
                          Card(),
                          Card(),
                          Card(),
                        ],
                      ),
                      SizedBox(
                        height: 40.0,
                      )
                    ]),
              ),
            ),
          );
        },
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
              padding: EdgeInsets.all(14),
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 3.0,
                    spreadRadius: 1.0,
                  ),
                ],
                borderRadius: BorderRadius.circular(50.0),
              ),
              child: Row(
                children: [
                  Text('0/'),
                  SizedBox(width: 5),
                  Text(
                    '123',
                    style: TextStyle(color: Colors.pink),
                  ),
                ],
              )),
          SizedBox(width: 10),
          FloatingActionButton.extended(
            onPressed: () {},
            label: Text('Save'),
            icon: Icon(
              Icons.bookmark,
              color: Colors.white,
            ),
            backgroundColor: Color(0xff1a237e),
          ),
        ],
      ),
    );
  }
}

class Card extends StatefulWidget {
  @override
  _CardState createState() => _CardState();
}

class _CardState extends State<Card> {
  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.pink;
      }
      return Colors.pink;
    }

    return Container(
        child: Stack(children: [
      Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/img/rice.png',
                width: 60,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text('Nasi'),
              SizedBox(
                height: 6.0,
              ),
              RichText(
                text: TextSpan(
                  text: '10 ',
                  style: TextStyle(color: Colors.orange, fontSize: 12.0),
                  children: <TextSpan>[
                    TextSpan(
                        text: '(gram)',
                        style: TextStyle(color: Colors.black45)),
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  text: '109 ',
                  style: TextStyle(color: Colors.blue, fontSize: 12.0),
                  children: <TextSpan>[
                    TextSpan(
                        text: '(Kalori)',
                        style: TextStyle(color: Colors.black45)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      Checkbox(
        checkColor: Colors.white,
        fillColor: MaterialStateProperty.resolveWith(getColor),
        value: isChecked,
        onChanged: (bool value) {
          setState(() {
            isChecked = value;
          });
        },
      ),
    ]));
  }
}
