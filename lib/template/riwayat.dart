import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:skrip_go/template/detail.dart';

class Riwayat extends StatefulWidget {
  @override
  _RiwayatState createState() => _RiwayatState();
}

class _RiwayatState extends State<Riwayat> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'History',
          style: TextStyle(color: Colors.black),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              LineIcons.powerOff,
              color: Colors.black,
            ),
            onPressed: () {},
          ),
        ],
        backgroundColor: Colors.white,
        elevation: 0,
        automaticallyImplyLeading: false,
      ),
      body: new LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints:
                  BoxConstraints(minHeight: viewportConstraints.maxHeight),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListView(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          children: <Widget>[
                            Card(),
                            Card(),
                            Card(),
                          ]),
                      SizedBox(height: 60)
                    ]),
              ),
            ),
          );
        },
      ),
    );
  }
}

class Card extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        // padding: EdgeInsets.all(10.0),
        margin: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 3.0,
              spreadRadius: 1.0,
            ),
          ],
        ),
        child: Material(
          borderRadius: BorderRadius.circular(20.0),
          color: Colors.white,
          child: InkWell(
            borderRadius: BorderRadius.circular(20.0),
            splashColor: Colors.blue,
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Detail()),
              );
            },
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 3.0,
                          spreadRadius: .0,
                          offset: Offset(
                            1.0,
                            2.0,
                          ),
                        ),
                      ],
                      image: DecorationImage(
                        image: AssetImage('assets/img/icon2.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    height: 60.0,
                    width: 60.0,
                  ),
                  SizedBox(width: 20.0),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('My Family',
                            style: TextStyle(
                              color: Color(0xFF3D3624),
                              fontWeight: FontWeight.bold,
                            )),
                        SizedBox(
                          height: 2.0,
                        ),
                        Text(
                          '21/02/2021',
                          style: TextStyle(fontSize: 13, color: Colors.black45),
                        )
                      ],
                    ),
                  ),
                  Icon(
                    Icons.chevron_right,
                    size: 15,
                    color: Colors.blue,
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
