import 'package:flutter/material.dart';

class Detail extends StatefulWidget {
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          '',
          style: TextStyle(color: Colors.black),
        ),
        leading: BackButton(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: new LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints:
                  BoxConstraints(minHeight: viewportConstraints.maxHeight),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          child: Row(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black26,
                                  blurRadius: 3.0,
                                  spreadRadius: .0,
                                  offset: Offset(
                                    1.0,
                                    2.0,
                                  ),
                                ),
                              ],
                              image: DecorationImage(
                                image: AssetImage('assets/img/icon2.png'),
                                fit: BoxFit.cover,
                              ),
                            ),
                            height: 60.0,
                            width: 60.0,
                          ),
                          SizedBox(width: 20.0),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('Nama',
                                    style: TextStyle(
                                      color: Color(0xFF3D3624),
                                      fontWeight: FontWeight.bold,
                                    )),
                                SizedBox(
                                  height: 2.0,
                                ),
                                Text(
                                  '21/02/2021',
                                  style: TextStyle(
                                      fontSize: 13, color: Colors.black45),
                                )
                              ],
                            ),
                          ),
                        ],
                      )),
                      SizedBox(height: 30),
                      Row(
                        children: [
                          Expanded(
                              child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.pink,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.pink),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Kebutuhan Kalori',
                                          style: TextStyle(
                                              color: Colors.pink,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text('123',
                                            style: TextStyle(
                                                color: Colors.pink,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                          SizedBox(width: 15),
                          Expanded(
                              child: Container(
                            // padding: EdgeInsets.all(10),

                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.green),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Karbohidrat',
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text('123',
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                        ],
                      ),
                      SizedBox(height: 15),
                      Row(
                        children: [
                          Expanded(
                              child: Container(
                            // padding: EdgeInsets.all(10),

                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.blue),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Protein',
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text('123',
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                          SizedBox(width: 15),
                          Expanded(
                              child: Container(
                            // padding: EdgeInsets.all(10),

                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.amber),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Lemak',
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text('123',
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        'Makanan yang dikomsumsi',
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      GridView.count(
                        primary: false,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        crossAxisCount: 2,
                        shrinkWrap: true,
                        children: <Widget>[
                          Card(),
                          Card(),
                          Card(),
                          Card(),
                          Card(),
                          Card(),
                          Card(),
                          Card(),
                          Card(),
                        ],
                      ),
                      SizedBox(
                        height: 40.0,
                      )
                    ]),
              ),
            ),
          );
        },
      ),
    );
  }
}

class Card extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Image.asset(
            'assets/img/rice.png',
            width: 60,
          ),
          SizedBox(
            height: 10.0,
          ),
          Text('Nasi'),
          SizedBox(
            height: 6.0,
          ),
          RichText(
            text: TextSpan(
              text: '10 ',
              style: TextStyle(color: Colors.orange, fontSize: 12.0),
              children: <TextSpan>[
                TextSpan(
                    text: '(gram)', style: TextStyle(color: Colors.black45)),
              ],
            ),
          ),
          RichText(
            text: TextSpan(
              text: '109 ',
              style: TextStyle(color: Colors.blue, fontSize: 12.0),
              children: <TextSpan>[
                TextSpan(
                    text: '(Kalori)', style: TextStyle(color: Colors.black45)),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
