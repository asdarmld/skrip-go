import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:skrip_go/template/beranda.dart';
import 'package:skrip_go/template/riwayat.dart';

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  int _selectedNavbar = 0;
  final _layoutPage = [Beranda(), Riwayat()];

  void _changeSelectedNavBar(int index) {
    setState(() {
      _selectedNavbar = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: _layoutPage.elementAt(_selectedNavbar),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          // ignore: deprecated_member_use
          BottomNavigationBarItem(icon: Icon(LineIcons.home), title: Text('')),
          BottomNavigationBarItem(
              // ignore: deprecated_member_use
              icon: Icon(Icons.bookmark_border),
              // ignore: deprecated_member_use
              title: Text('')),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedNavbar,
        selectedItemColor: Color(0xff1a237e),
        // elevation: 0,
        onTap: (index) {
          setState(() {
            _selectedNavbar = index;
          });
        },
        backgroundColor: Colors.white,
        unselectedItemColor: Colors.black54,
        showSelectedLabels: false,
        showUnselectedLabels: false,
      ),
    );
  }
}
