part of 'pages.dart';

class CountPage extends StatefulWidget {
  final TotalFamily totalFamily;
  CountPage(this.totalFamily);

  @override
  _CountPageState createState() => _CountPageState();
}

class _CountPageState extends State<CountPage> {
  @override
  Widget build(BuildContext context) {
    context.bloc<RekomendasiCubit>().getRekomendasi();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          '',
          style: TextStyle(color: Colors.black),
        ),
        leading: BackButton(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: new LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints:
                  BoxConstraints(minHeight: viewportConstraints.maxHeight),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                              child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.pink,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.pink),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Kebutuhan Kalori',
                                          style: TextStyle(
                                              color: Colors.pink,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text(widget.totalFamily.energi,
                                            style: TextStyle(
                                                color: Colors.pink,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                        ],
                      ),
                      SizedBox(height: 15),
                      Row(
                        children: [
                          Expanded(
                              child: Container(
                            // padding: EdgeInsets.all(10),

                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.green),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Karbohidrat',
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text(widget.totalFamily.karbohidrat,
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                          SizedBox(width: 15),
                          Expanded(
                              child: Container(
                            // padding: EdgeInsets.all(10),

                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.blue),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Protein',
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text(widget.totalFamily.protein,
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                          SizedBox(width: 15),
                          Expanded(
                              child: Container(
                            // padding: EdgeInsets.all(10),

                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.amber),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Lemak',
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text(widget.totalFamily.lemak,
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      RichText(
                        text: TextSpan(
                          text:
                              'Berikut adalah rekomendasi makanan berdasarkan hasil perhitungan kalori anda, ',
                          style: TextStyle(
                            color: Colors.black54,
                          ),
                          children: <TextSpan>[
                            TextSpan(
                                text: 'Pilih makanan sendiri ?',
                                style: TextStyle(color: Colors.blue),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ChoicePage(widget.totalFamily)),
                                    );
                                    // Navigator.push(
                                    //   context,
                                    //   MaterialPageRoute(
                                    //       builder: (context) => Choice()),
                                    // );
                                  }),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        'Rekomendasi Makanan',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      BlocBuilder<RekomendasiCubit, RekomendasiState>(
                        builder: (context, state) {
                          return (state is RekomendasiLoaded)
                              ? GridView.count(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 3, vertical: 10),
                                  primary: false,
                                  crossAxisSpacing: 20,
                                  mainAxisSpacing: 20,
                                  crossAxisCount: 2,
                                  shrinkWrap: true,
                                  children: state.rekomendasi
                                      .map((e) => RekomendasiFood(e))
                                      .toList(),
                                )
                              : loadingIndicator;
                        },
                      ),
                      SizedBox(
                        height: 40.0,
                      )
                    ]),
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          CaloriesServices.simpanRekomendasi().then((value) =>
              (value.message == 'Berhasil disimpan')
                  ? notifBerhasil(context, 'Berhasil disimpan')
                      .then(Timer(Duration(seconds: 2), () {
                      // 5 seconds over, navigate to Page2.
                      // context.bloc<RiwayatCubit>().getRiwayat();
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MainPage(
                                  bottomNavBarIndex: 1,
                                )),
                      );
                    }))
                  : notifGagal(context, value.message));
        },
        label: Text('Save'),
        icon: Icon(
          Icons.bookmark,
          color: Colors.white,
        ),
        backgroundColor: Color(0xff1a237e),
      ),
    );
  }
}
