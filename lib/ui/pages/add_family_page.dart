part of 'pages.dart';

class AddFamilyPage extends StatefulWidget {
  @override
  _AddFamilyPageState createState() => _AddFamilyPageState();
}

class _AddFamilyPageState extends State<AddFamilyPage> {
  TextEditingController namaController = TextEditingController();
  TextEditingController usiaController = TextEditingController();
  TextEditingController tinggiController = TextEditingController();
  TextEditingController beratBadanController = TextEditingController();

  bool isSend = false;

  String selectedGander;
  String selectedActivity;

  @override
  void initState() {
    super.initState();
    selectedGander = 'Laki-laki';
    selectedActivity = '1.2';
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        // ignore: deprecated_member_use
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => MainPage()));
        return;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            '',
            style: TextStyle(color: Colors.black),
          ),
          leading: BackButton(
            color: Colors.black,
          ),
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: new LayoutBuilder(
          builder: (BuildContext context, BoxConstraints viewportConstraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints:
                    BoxConstraints(minHeight: viewportConstraints.maxHeight),
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextField(
                          controller: namaController,
                          decoration: InputDecoration(
                            labelText: 'Nama',
                            labelStyle: TextStyle(fontSize: 15),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.blue,
                              ),
                            ),
                          ),
                        ),
                        TextField(
                          keyboardType: TextInputType.number,
                          controller: usiaController,
                          decoration: InputDecoration(
                            labelText: 'Usia (Tahun)',
                            labelStyle: TextStyle(fontSize: 15),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.blue,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        TextField(
                          keyboardType: TextInputType.number,
                          controller: tinggiController,
                          decoration: InputDecoration(
                            labelText: 'Tinggi (CM)',
                            labelStyle: TextStyle(fontSize: 15),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.blue,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        TextField(
                          keyboardType: TextInputType.number,
                          controller: beratBadanController,
                          decoration: InputDecoration(
                            labelText: 'Berat Badan (KG)',
                            labelStyle: TextStyle(fontSize: 15),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.blue,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 30),
                        Text('Jenis kelamin',
                            style: TextStyle(fontWeight: FontWeight.w600)),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            Radio(
                              value: 'Laki-laki',
                              groupValue: selectedGander,
                              onChanged: (val) {
                                setState(() {
                                  selectedGander = val;
                                });
                              },
                              activeColor: Colors.blue,
                            ),
                            Text('Laki-laki',
                                style: TextStyle(color: Colors.black54))
                          ],
                        ),
                        Row(
                          children: [
                            Radio(
                              value: 'Perempuan',
                              groupValue: selectedGander,
                              onChanged: (val) {
                                setState(() {
                                  selectedGander = val;
                                });
                              },
                              activeColor: Colors.pink,
                            ),
                            Text(
                              'Perempuan',
                              style: TextStyle(color: Colors.black54),
                            )
                          ],
                        ),
                        SizedBox(height: 30),
                        Text('Faktor',
                            style: TextStyle(fontWeight: FontWeight.w600)),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            Radio(
                              value: '1.2',
                              groupValue: selectedActivity,
                              onChanged: (act) {
                                setState(() {
                                  selectedActivity = act;
                                });
                              },
                              activeColor: Colors.blue,
                            ),
                            Text('Tidak aktif',
                                style: TextStyle(color: Colors.black54))
                          ],
                        ),
                        Row(
                          children: [
                            Radio(
                              value: '1.3',
                              groupValue: selectedActivity,
                              onChanged: (act) {
                                setState(() {
                                  selectedActivity = act;
                                });
                              },
                              activeColor: Colors.blue,
                            ),
                            Text('Cukup aktif',
                                style: TextStyle(color: Colors.black54))
                          ],
                        ),
                        Row(
                          children: [
                            Radio(
                              value: '1.4',
                              groupValue: selectedActivity,
                              onChanged: (act) {
                                setState(() {
                                  selectedActivity = act;
                                });
                              },
                              activeColor: Colors.blue,
                            ),
                            Text('Aktif',
                                style: TextStyle(color: Colors.black54))
                          ],
                        ),
                        Row(
                          children: [
                            Radio(
                              value: '1.5',
                              groupValue: selectedActivity,
                              onChanged: (act) {
                                setState(() {
                                  selectedActivity = act;
                                });
                              },
                              activeColor: Colors.blue,
                            ),
                            Text('Sangat aktif',
                                style: TextStyle(color: Colors.black54))
                          ],
                        ),
                        SizedBox(height: 60),
                      ]),
                ),
              ),
            );
          },
        ),
        floatingActionButton: isSend
            ? SpinKitCircle(
                color: Colors.blue,
              )
            : FloatingActionButton.extended(
                onPressed: () async {
                  if (!(namaController.text.trim() != "" &&
                      usiaController.text.trim() != "" &&
                      tinggiController.text.trim() != "" &&
                      beratBadanController.text.trim() != "" &&
                      selectedGander != "" &&
                      selectedActivity != "")) {
                    Flushbar(
                      duration: Duration(milliseconds: 1500),
                      flushbarPosition: FlushbarPosition.TOP,
                      backgroundColor: Colors.red,
                      message: "Please fill all the fields",
                    )..show(context);

                    // next
                  } else {
                    setState(() {
                      isSend = true;
                    });

                    await context.bloc<FamilyCubit>().submitFamily(
                          namaController.text.trim(),
                          tinggiController.text.trim(),
                          usiaController.text.trim(),
                          beratBadanController.text.trim(),
                          selectedGander,
                          selectedActivity,
                        );
                    FamilyState result = context.bloc<FamilyCubit>().state;

                    if (result is FamilyLoaded) {
                      context.bloc<TotalCubit>().getTotal();
                      setState(() {
                        isSend = false;
                      });
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) => MainPage()));
                    } else {
                      setState(() {
                        isSend = false;
                      });
                      notifGagal(
                          context, (result as FamilyLoadingFailed).message);
                    }
                  }
                },
                label: Text('Simpan'),
                icon: Icon(
                  Icons.bookmark,
                  color: Colors.white,
                ),
                backgroundColor: Color(0xff1a237e),
              ),
      ),
    );
  }
}
