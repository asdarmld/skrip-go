part of 'pages.dart';

class SignUpPage extends StatefulWidget {
  // final RegistrationData registrationData;
  // SignUpPage(this.registrationData);
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController retypePasswordController = TextEditingController();
  TextEditingController hpController = TextEditingController();

  bool isEmailValid = false;
  bool isPasswordValid = false;
  bool isSigningUp = false;

  // @override
  // void initState() {
  //   super.initState();

  //   nameController.text = widget.registrationData.name;
  //   emailController.text = widget.registrationData.email;
  //   hpController.text = widget.registrationData.hp;
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff1a237e),
      body: SafeArea(child: new LayoutBuilder(
          builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints:
                BoxConstraints(minHeight: viewportConstraints.maxHeight),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Center(
                      child: Column(
                    children: [
                      Text('SMART GROCERY',
                          style: TextStyle(
                              fontSize: 30,
                              letterSpacing: 3,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                      SizedBox(
                        height: 10.0,
                      ),
                      RichText(
                        text: TextSpan(
                          text: 'App For ',
                          style: TextStyle(color: Colors.white, fontSize: 18),
                          children: <TextSpan>[
                            TextSpan(
                                text: 'Family Safe and Healthy',
                                style: TextStyle(color: Colors.blue)),
                          ],
                        ),
                      ),
                    ],
                  )),
                  SizedBox(height: 15.0),
                  Container(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Sign Up',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      TextField(
                        controller: nameController,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          labelText: 'Nama',
                          labelStyle:
                              TextStyle(color: Colors.white.withOpacity(.5)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextField(
                        controller: hpController,
                        keyboardType: TextInputType.number,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          labelText: 'No Hp',
                          labelStyle:
                              TextStyle(color: Colors.white.withOpacity(.5)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextField(
                        controller: emailController,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          labelText: 'E-mail',
                          labelStyle:
                              TextStyle(color: Colors.white.withOpacity(.5)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      TextField(
                        controller: passwordController,
                        style: TextStyle(color: Colors.white),
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: 'Password',
                          labelStyle:
                              TextStyle(color: Colors.white.withOpacity(.5)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      TextField(
                        controller: retypePasswordController,
                        style: TextStyle(color: Colors.white),
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: 'Konfirmasi password',
                          labelStyle:
                              TextStyle(color: Colors.white.withOpacity(.5)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
                  SizedBox(height: 15.0),
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () async {
                      if (!(nameController.text.trim() != "" &&
                          emailController.text.trim() != "" &&
                          passwordController.text.trim() != "" &&
                          retypePasswordController.text.trim() != "" &&
                          hpController.text.trim() != "")) {
                        notifGagal(context, 'Please fill all the fields');
                      } else if (passwordController.text !=
                          retypePasswordController.text) {
                        notifGagal(context,
                            'Mismatch password and confirmed password');
                      } else if (passwordController.text.length < 8) {
                        notifGagal(
                            context, "Password's length min 8 characters");
                      } else if (hpController.text.length < 11) {
                        notifGagal(context, "Hp length min 11 characters");
                      } else if (!EmailValidator.validate(
                          emailController.text)) {
                        notifGagal(context, "Wrong formatted email address");
                      } else {
                        setState(() {
                          isSigningUp = true;
                        });

                        await context.bloc<UserCubit>().register(
                            nameController.text.trim(),
                            emailController.text.trim(),
                            passwordController.text.trim(),
                            hpController.text.toString());
                        UserState state = context.bloc<UserCubit>().state;
                        if (state is UserLoaded) {
                          setState(() {
                            isSigningUp = false;
                          });
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => MainPage()),
                          );
                        } else {
                          setState(() {
                            isSigningUp = false;
                          });
                          notifGagal(
                              context, (state as UserLoadingFailed).message);
                        }
                      }

                      //
                    },
                    child: Container(
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: Colors.white),
                      child: Icon(
                        Icons.chevron_right,
                        color: Color(0xff1a237e),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      })),
    );
  }
}
