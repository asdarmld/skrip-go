part of 'pages.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isEmailValid = false;
  bool isPasswordValid = false;
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff1a237e),
      body: SafeArea(child: new LayoutBuilder(
          builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints:
                BoxConstraints(minHeight: viewportConstraints.maxHeight),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Center(
                      child: Column(
                    children: [
                      Text('SMART GROCERY',
                          style: TextStyle(
                              fontSize: 30,
                              letterSpacing: 3,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                      SizedBox(
                        height: 10.0,
                      ),
                      RichText(
                        text: TextSpan(
                          text: 'App For ',
                          style: TextStyle(color: Colors.white, fontSize: 18),
                          children: <TextSpan>[
                            TextSpan(
                                text: 'Family Safe and Healthy',
                                style: TextStyle(color: Colors.blue)),
                          ],
                        ),
                      ),
                    ],
                  )),
                  SizedBox(height: 15.0),
                  Container(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Sign In',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      TextField(
                        controller: emailController,
                        onChanged: (text) {
                          setState(() {
                            isEmailValid = EmailValidator.validate(text);
                          });
                        },
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          labelText: 'E-mail',
                          labelStyle:
                              TextStyle(color: Colors.white.withOpacity(.5)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      TextField(
                        controller: passwordController,
                        onChanged: (text) {
                          setState(() {
                            isPasswordValid = text.length >= 6;
                          });
                        },
                        style: TextStyle(color: Colors.white),
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: 'Password',
                          labelStyle:
                              TextStyle(color: Colors.white.withOpacity(.5)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      RichText(
                        text: TextSpan(
                          text: 'Dont have account?  ',
                          style: TextStyle(
                            color: Colors.blue,
                          ),
                          children: <TextSpan>[
                            TextSpan(
                                text: 'Sign Up',
                                style: TextStyle(color: Colors.white),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => SignUpPage()),
                                      )),
                          ],
                        ),
                      ),
                    ],
                  )),
                  SizedBox(height: 15.0),
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () async {
                      setState(() {
                        isLoading = true;
                      });

                      if (passwordController.text != "" &&
                          emailController.text != "") {
                        await context.bloc<UserCubit>().signIn(
                            emailController.text, passwordController.text);
                        UserState state = context.bloc<UserCubit>().state;
                        if (state is UserLoaded) {
                          context.bloc<FamilyCubit>().getFamily();
                          context.bloc<TotalCubit>().getTotal();
                          context.bloc<CaloriesCubit>().getCalories();
                          context.bloc<RiwayatCubit>().getRiwayat();
                          setState(() {
                            isLoading = false;
                          });

                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MainPage()));
                        } else {
                          setState(() {
                            isLoading = false;
                          });
                          notifGagal(
                              context, (state as UserLoadingFailed).message);
                        }
                      } else {
                        setState(() {
                          isLoading = false;
                        });
                        notifGagal(context, 'Please fill all the fields');
                      }
                    },
                    child: isLoading
                        ? loadingIndicator
                        : Container(
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color: Colors.white),
                            child: Icon(
                              Icons.chevron_right,
                              color: Color(0xff1a237e),
                            ),
                          ),
                  )
                ],
              ),
            ),
          ),
        );
      })),
    );
  }
}
