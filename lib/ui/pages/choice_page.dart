part of 'pages.dart';

class ChoicePage extends StatefulWidget {
  final TotalFamily totalFamily;
  ChoicePage(this.totalFamily);

  @override
  _ChoicePageState createState() => _ChoicePageState();
}

class _ChoicePageState extends State<ChoicePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          '',
          style: TextStyle(color: Colors.black),
        ),
        leading: BackButton(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: new LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints:
                  BoxConstraints(minHeight: viewportConstraints.maxHeight),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                              child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.pink,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.pink),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Kebutuhan Kalori',
                                          style: TextStyle(
                                              color: Colors.pink,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text(widget.totalFamily.energi,
                                            style: TextStyle(
                                                color: Colors.pink,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                        ],
                      ),
                      SizedBox(height: 15),
                      Row(
                        children: [
                          Expanded(
                              child: Container(
                            // padding: EdgeInsets.all(10),

                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.green),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Karbohidrat',
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text(widget.totalFamily.karbohidrat,
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                          SizedBox(width: 15),
                          Expanded(
                              child: Container(
                            // padding: EdgeInsets.all(10),

                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.blue),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Protein',
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text(widget.totalFamily.protein,
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                          SizedBox(width: 15),
                          Expanded(
                              child: Container(
                            // padding: EdgeInsets.all(10),

                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(15),
                                          bottomLeft: Radius.circular(17),
                                        ),
                                        //  borderRadius: BorderRadius.circular(15.0),
                                        color: Colors.amber),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                          'Lemak',
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 11.0),
                                        ),
                                        SizedBox(height: 8),
                                        Text(widget.totalFamily.lemak,
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      RichText(
                        text: TextSpan(
                          text:
                              'Silahkan memilih makanan sesuai dengan hasil perhitungan total kebutuhan kalori keluarga anda ',
                          style: TextStyle(color: Colors.black54, height: 1.5),
                          children: <TextSpan>[
                            TextSpan(
                              text:
                                  ', makanan yang dipilih tidak boleh melebihi kelori total',
                              style: TextStyle(color: Colors.black54),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      BlocBuilder<CaloriesCubit, CaloriesState>(
                        builder: (context, stateCalories) {
                          return (stateCalories is CaloriesLoaded)
                              ? GridView.count(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 3, vertical: 10),
                                  primary: false,
                                  crossAxisSpacing: 20,
                                  mainAxisSpacing: 20,
                                  crossAxisCount: 2,
                                  shrinkWrap: true,
                                  children: stateCalories.calories
                                      .map((e) =>
                                          ChoiceCardPage(e, widget.totalFamily))
                                      .toList(),
                                )
                              : loadingIndicator;
                        },
                      ),
                      SizedBox(
                        height: 40.0,
                      )
                    ]),
              ),
            ),
          );
        },
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          // Container(
          //     padding: EdgeInsets.all(14),
          //     decoration: BoxDecoration(
          //       color: Colors.white,
          //       boxShadow: [
          //         BoxShadow(
          //           color: Colors.grey,
          //           blurRadius: 3.0,
          //           spreadRadius: 1.0,
          //         ),
          //       ],
          //       borderRadius: BorderRadius.circular(50.0),
          //     ),
          //     child: Row(
          //       children: [
          //         Text(sum.toString() + ' /'),
          //         SizedBox(width: 5),
          //         Text(
          //           widget.totalFamily.energi,
          //           style: TextStyle(color: Colors.pink),
          //         ),
          //       ],
          //     )),
          SizedBox(width: 10),
          FloatingActionButton.extended(
            onPressed: () {
              CaloriesServices.simpanPilihan().then((value) =>
                  (value.message == 'Berhasil disimpan')
                      ? notifBerhasil(context, 'Berhasil disimpan')
                          .then(Timer(Duration(seconds: 2), () {
                          pilihan.clear();
                          // context.bloc<RiwayatCubit>().getRiwayat();
                          // 5 seconds over, navigate to Page2.
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MainPage(
                                      bottomNavBarIndex: 1,
                                    )),
                          );
                        }))
                      : notifGagal(context, value.message));
            },
            label: Text('Save'),
            icon: Icon(
              Icons.bookmark,
              color: Colors.white,
            ),
            backgroundColor: Color(0xff1a237e),
          ),
        ],
      ),
    );
  }
}
