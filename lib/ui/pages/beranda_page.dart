part of 'pages.dart';

class BerandaPage extends StatefulWidget {
  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        // title: Text(
        //   'SMART GROCERY',
        //   style: TextStyle(color: Colors.black),
        // ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              LineIcons.powerOff,
              color: Colors.black,
            ),
            onPressed: () async {
              SharedPreferences preferences =
                  await SharedPreferences.getInstance();
              preferences.setString("access_token", "");

              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => SignInPage()),
              );
            },
          ),
        ],
        backgroundColor: Colors.white,
        elevation: 0,
        automaticallyImplyLeading: false,
      ),
      body: new LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) {
          return SingleChildScrollView(
              child: BlocBuilder<TotalCubit, TotalState>(
            builder: (context, family) {
              if (family is FamilyTotalLoaded) {
                // TotalFamily total = family.totalFamily;
                return ConstrainedBox(
                  constraints:
                      BoxConstraints(minHeight: viewportConstraints.maxHeight),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Container(
                              padding:
                                  EdgeInsets.only(right: 10, top: 10, left: 10),
                              margin: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                color: Color(0xff1a237e),
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Hitung kalori harian keluarga anda',
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.white),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Text(
                                            'Tambahkan data keluarga anda dan dapatkan data makanan rekomendasi untuk memenuhi kebutuhan kalori',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 11),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  Image.asset(
                                    'assets/img/3d1.png',
                                    width: 80,
                                  )
                                ],
                              )),
                        ),

                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0),
                          child: Row(
                            children: [
                              Expanded(
                                  child: Container(
                                // padding: EdgeInsets.all(10),
                                margin: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.pink,
                                    width: 1.0,
                                  ),
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                child: Stack(
                                  children: [
                                    Positioned(
                                      top: 0,
                                      right: 0,
                                      child: Container(
                                        width: 15,
                                        height: 15,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(15),
                                              bottomLeft: Radius.circular(17),
                                            ),
                                            //  borderRadius: BorderRadius.circular(15.0),
                                            color: Colors.pink),
                                      ),
                                    ),
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Column(
                                          children: [
                                            Text(
                                              'Kebutuhan Kalori',
                                              style: TextStyle(
                                                  color: Colors.pink,
                                                  fontSize: 11.0),
                                            ),
                                            SizedBox(height: 8),
                                            Text(family.totalFamily.energi,
                                                style: TextStyle(
                                                    color: Colors.pink,
                                                    fontSize: 20,
                                                    fontWeight:
                                                        FontWeight.w600))
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0),
                          child: Row(
                            children: [
                              Expanded(
                                  child: Container(
                                // padding: EdgeInsets.all(10),
                                margin: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black38,
                                    width: 1.0,
                                  ),
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                child: Stack(
                                  children: [
                                    Positioned(
                                      top: 0,
                                      right: 0,
                                      child: Container(
                                        width: 15,
                                        height: 15,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(15),
                                              bottomLeft: Radius.circular(17),
                                            ),
                                            //  borderRadius: BorderRadius.circular(15.0),
                                            color: Colors.green),
                                      ),
                                    ),
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Column(
                                          children: [
                                            Text(
                                              'Karbohidrat',
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 11.0),
                                            ),
                                            SizedBox(height: 8),
                                            Text(family.totalFamily.karbohidrat,
                                                style: TextStyle(
                                                    color: Colors.black87,
                                                    fontSize: 20,
                                                    fontWeight:
                                                        FontWeight.w600))
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                              Expanded(
                                  child: Container(
                                // padding: EdgeInsets.all(10),
                                margin: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black38,
                                    width: 1.0,
                                  ),
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                child: Stack(
                                  children: [
                                    Positioned(
                                      top: 0,
                                      right: 0,
                                      child: Container(
                                        width: 15,
                                        height: 15,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(15),
                                              bottomLeft: Radius.circular(17),
                                            ),
                                            //  borderRadius: BorderRadius.circular(15.0),
                                            color: Colors.blue),
                                      ),
                                    ),
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Column(
                                          children: [
                                            Text(
                                              'Protein',
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 11.0),
                                            ),
                                            SizedBox(height: 8),
                                            Text(family.totalFamily.protein,
                                                style: TextStyle(
                                                    color: Colors.black87,
                                                    fontSize: 20,
                                                    fontWeight:
                                                        FontWeight.w600))
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                              Expanded(
                                  child: Container(
                                // padding: EdgeInsets.all(10),
                                margin: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black38,
                                    width: 1.0,
                                  ),
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                child: Stack(
                                  children: [
                                    Positioned(
                                      top: 0,
                                      right: 0,
                                      child: Container(
                                        width: 15,
                                        height: 15,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(15),
                                              bottomLeft: Radius.circular(17),
                                            ),
                                            //  borderRadius: BorderRadius.circular(15.0),
                                            color: Colors.amber),
                                      ),
                                    ),
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Column(
                                          children: [
                                            Text(
                                              'Lemak',
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 11.0),
                                            ),
                                            SizedBox(height: 8),
                                            Text(family.totalFamily.lemak,
                                                style: TextStyle(
                                                    color: Colors.black87,
                                                    fontSize: 20,
                                                    fontWeight:
                                                        FontWeight.w600))
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                            ],
                          ),
                        ),
                        SizedBox(height: 30),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: Text(
                            'My Family',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 10),
                        BlocBuilder<FamilyCubit, FamilyState>(
                          builder: (context, stateFamily) {
                            return (stateFamily is FamilyLoaded)
                                ? ListView(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 20),
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    children: stateFamily.family
                                        .map((e) => CardFamily(e))
                                        .toList(),
                                  )
                                : Center(
                                    child: SpinKitCircle(
                                      color: Colors.red,
                                    ),
                                  );
                          },
                        ),
                        // : Center(
                        //     child: SpinKitCircle(
                        //       color: Colors.blue,
                        //     ),
                        //   )),
                        Container(
                          margin: EdgeInsets.all(20),
                          padding: EdgeInsets.all(15),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                            color: Color(0xff1a237e),
                          ),
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddFamilyPage()));
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.add, color: Colors.white),
                                SizedBox(width: 10),
                                Text(
                                  'Add',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          padding: EdgeInsets.all(13),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xff1a237e),
                              width: 1.0,
                            ),
                          ),
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        CountPage(family.totalFamily)),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.fastfood_rounded,
                                    color: Colors.pink),
                                SizedBox(width: 10),
                                Text(
                                  'Creat food',
                                  style: TextStyle(
                                    color: Color(0xff1a237e),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 30),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: Text(
                            'Foods',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        BlocBuilder<CaloriesCubit, CaloriesState>(
                          builder: (context, stateCalories) {
                            return (stateCalories is CaloriesLoaded)
                                ? GridView.count(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                    primary: false,
                                    crossAxisSpacing: 20,
                                    mainAxisSpacing: 20,
                                    crossAxisCount: 2,
                                    shrinkWrap: true,
                                    children: stateCalories.calories
                                        .map((e) => CardFood(e))
                                        .take(10)
                                        .toList(),
                                  )
                                : loadingIndicator;
                          },
                        ),
                      ]),
                );
              } else {
                return Container(
                  child: Center(
                    child: SpinKitCircle(
                      color: Colors.grey,
                    ),
                  ),
                );
              }
            },
          ));
        },
      ),
    );
  }
}

// class CardFood extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {

//   }
// }
