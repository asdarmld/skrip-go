part of 'pages.dart';

class MainPage extends StatefulWidget {
  final int bottomNavBarIndex;

  MainPage({this.bottomNavBarIndex = 0});
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int bottomNavBarIndex;
  final _layoutPage = [BerandaPage(), RiwayatPage()];

  @override
  void initState() {
    super.initState();

    bottomNavBarIndex = widget.bottomNavBarIndex;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: _layoutPage.elementAt(bottomNavBarIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          // ignore: deprecated_member_use
          BottomNavigationBarItem(icon: Icon(LineIcons.home), title: Text('')),
          BottomNavigationBarItem(
              // ignore: deprecated_member_use
              icon: Icon(Icons.bookmark_border),
              // ignore: deprecated_member_use
              title: Text('')),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: bottomNavBarIndex,
        selectedItemColor: Color(0xff1a237e),
        // elevation: 0,
        onTap: (index) {
          setState(() {
            bottomNavBarIndex = index;
          });
        },
        backgroundColor: Colors.white,
        unselectedItemColor: Colors.black54,
        showSelectedLabels: false,
        showUnselectedLabels: false,
      ),
    );
  }
}
