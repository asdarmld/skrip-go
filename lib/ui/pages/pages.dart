import 'dart:async';

import 'package:another_flushbar/flushbar.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../cubit/cubit.dart';
import '../../model/models.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../services/services.dart';
import '../../shared/shared.dart';
import '../widgets/widgets.dart';

part 'sign_in_page.dart';
part 'sign_up_page.dart';
part 'main_page.dart';
part 'riwayat_page.dart';
part 'beranda_page.dart';
part 'count_page.dart';
part 'add_family_page.dart';
part 'choice_page.dart';
part 'detail_riwayat_page.dart';
