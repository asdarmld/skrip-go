part of 'widgets.dart';

class RekomendasiFood extends StatelessWidget {
  final RekomendasiModel caloriesModel;
  RekomendasiFood(this.caloriesModel);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 3.0,
            spreadRadius: 1.0,
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FadeInImage.assetNetwork(
            placeholder: 'assets/img/loading.gif',
            image: assetsURL + "${caloriesModel.gambar}",
            width: 60,
          ),
          // Image.network(
          //   assetsURL + "${caloriesModel.gambar}",
          //   width: 60,
          // ),
          SizedBox(
            height: 10.0,
          ),
          Text(caloriesModel.nama, textAlign: TextAlign.center),
          SizedBox(
            height: 6.0,
          ),
          RichText(
            text: TextSpan(
              text: '100 ',
              style: TextStyle(color: Colors.orange, fontSize: 12.0),
              children: <TextSpan>[
                TextSpan(
                    text: '(gram)', style: TextStyle(color: Colors.black45)),
              ],
            ),
          ),
          RichText(
            text: TextSpan(
              text: caloriesModel.kalori.toString(),
              style: TextStyle(color: Colors.blue, fontSize: 12.0),
              children: <TextSpan>[
                TextSpan(
                    text: '(Kalori)', style: TextStyle(color: Colors.black45)),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
