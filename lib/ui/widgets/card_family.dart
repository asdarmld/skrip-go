part of 'widgets.dart';

class CardFamily extends StatelessWidget {
  final FamilyModel familyModel;
  CardFamily(this.familyModel);
  @override
  Widget build(BuildContext context) {
    return Container(
      // padding: EdgeInsets.all(10.0),
      margin: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 3.0,
            spreadRadius: 1.0,
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 3.0,
                    spreadRadius: .0,
                    offset: Offset(
                      1.0,
                      2.0,
                    ),
                  ),
                ],
                image: DecorationImage(
                  image: AssetImage((familyModel.gender == 'Perempuan')
                      ? 'assets/img/3d2.png'
                      : 'assets/img/pria.jpg'),
                  fit: BoxFit.fill,
                ),
              ),
              height: 60.0,
              width: 60.0,
            ),
            SizedBox(width: 20.0),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(familyModel.nama,
                      style: TextStyle(
                        color: Color(0xFF3D3624),
                        fontWeight: FontWeight.bold,
                      )),
                  SizedBox(
                    height: 4.0,
                  ),
                  Row(
                    children: [
                      Text(
                        double.parse(familyModel.energi).toStringAsFixed(0) +
                            '/',
                        style: TextStyle(fontSize: 12, color: Colors.pink),
                      ),
                      Text(
                        double.parse(familyModel.karbohidrat)
                                .toStringAsFixed(0) +
                            '/',
                        style: TextStyle(fontSize: 12, color: Colors.green),
                      ),
                      Text(
                        double.parse(familyModel.protein).toStringAsFixed(0) +
                            '/',
                        style: TextStyle(fontSize: 12, color: Colors.blue),
                      ),
                      Text(
                        double.parse(familyModel.lemak).toStringAsFixed(0),
                        style: TextStyle(fontSize: 12, color: Colors.amber),
                      ),
                    ],
                  )
                ],
              ),
            ),
            InkWell(
                onTap: () async {
                  await context
                      .bloc<FamilyCubit>()
                      .deleteFamily(familyModel.id);
                  FamilyState result = context.bloc<FamilyCubit>().state;

                  if (result is FamilyLoaded) {
                    context.bloc<TotalCubit>().getTotal();
                    notifBerhasil(context, 'Berhasil di hapus');
                  } else {
                    notifGagal(
                        context, (result as FamilyLoadingFailed).message);
                  }
                },
                child: Icon(
                  Icons.delete_outline,
                  color: Colors.grey[300],
                ))
          ],
        ),
      ),
    );
  }
}
