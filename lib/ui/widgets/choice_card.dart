part of 'widgets.dart';

class ChoiceCardPage extends StatefulWidget {
  final CaloriesModel caloriesModel;
  final TotalFamily totalFamily;
  ChoiceCardPage(this.caloriesModel, this.totalFamily);

  @override
  _ChoiceCardPageState createState() => _ChoiceCardPageState();
}

class _ChoiceCardPageState extends State<ChoiceCardPage> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.pink;
      }
      return Colors.pink;
    }

    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 3.0,
              spreadRadius: 1.0,
            ),
          ],
        ),
        child: Stack(children: [
          Center(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FadeInImage.assetNetwork(
                    placeholder: 'assets/img/loading.gif',
                    image: assetsURL + "${widget.caloriesModel.gambar}",
                    width: 60,
                  ),
                  // Image.network(
                  //   assetsURL + "${widget.caloriesModel.gambar}",
                  //   width: 60,
                  // ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(widget.caloriesModel.nama, textAlign: TextAlign.center),
                  SizedBox(
                    height: 6.0,
                  ),
                  RichText(
                    text: TextSpan(
                      text: '100 ',
                      style: TextStyle(color: Colors.orange, fontSize: 12.0),
                      children: <TextSpan>[
                        TextSpan(
                            text: '(gram)',
                            style: TextStyle(color: Colors.black45)),
                      ],
                    ),
                  ),
                  RichText(
                    text: TextSpan(
                      text: widget.caloriesModel.kalori.toString(),
                      style: TextStyle(color: Colors.blue, fontSize: 12.0),
                      children: <TextSpan>[
                        TextSpan(
                            text: ' (Kalori)',
                            style: TextStyle(color: Colors.black45)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Checkbox(
            checkColor: Colors.white,
            fillColor: MaterialStateProperty.resolveWith(getColor),
            value: (pilihan.contains(widget.caloriesModel.id)) ? true : false,
            onChanged: (bool value) {
              setState(() {
                if (value) {
                  if (sum < int.parse(widget.totalFamily.energi)) {
                    pilihan.add(widget.caloriesModel.id);
                    jumlahKalori.add(widget.caloriesModel.kalori);
                  } else {
                    notifGagal(context, 'Sudah melebihi jumlah kalori.');
                  }
                } else {
                  pilihan.remove(widget.caloriesModel.id);
                  jumlahKalori.remove(widget.caloriesModel.kalori);
                }

                cekJumlahKalori();
                // context.bloc<JumlahKaloriCubit>().getJumlahKalori();
                // print(pilihan);
                print(sum);
                // print(sum);
              });
            },
          ),
        ]));
  }
}
