part of 'widgets.dart';

class RiwayatCard extends StatelessWidget {
  final TotalRiwayatModel totalRiwayatModel;
  RiwayatCard(this.totalRiwayatModel);

  @override
  Widget build(BuildContext context) {
    return Container(
        // padding: EdgeInsets.all(10.0),
        margin: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 3.0,
              spreadRadius: 1.0,
            ),
          ],
        ),
        child: Material(
          borderRadius: BorderRadius.circular(20.0),
          color: Colors.white,
          child: InkWell(
            borderRadius: BorderRadius.circular(20.0),
            splashColor: Colors.blue,
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailPage(totalRiwayatModel)),
              );
            },
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 3.0,
                          spreadRadius: .0,
                          offset: Offset(
                            1.0,
                            2.0,
                          ),
                        ),
                      ],
                      image: DecorationImage(
                        image: AssetImage('assets/img/3d2.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    height: 60.0,
                    width: 60.0,
                  ),
                  SizedBox(width: 20.0),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('My Family',
                            style: TextStyle(
                              color: Color(0xFF3D3624),
                              fontWeight: FontWeight.bold,
                            )),
                        SizedBox(
                          height: 2.0,
                        ),
                        Text(
                          totalRiwayatModel.tgl,
                          style: TextStyle(fontSize: 13, color: Colors.black45),
                        )
                      ],
                    ),
                  ),
                  Icon(
                    Icons.chevron_right,
                    size: 15,
                    color: Colors.blue,
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
