import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './ui/pages/pages.dart';

import 'cubit/cubit.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => UserCubit()),
        BlocProvider(create: (_) => FamilyCubit()),
        BlocProvider(create: (_) => TotalCubit()),
        BlocProvider(create: (_) => CaloriesCubit()),
        BlocProvider(create: (_) => RekomendasiCubit()),
        BlocProvider(create: (_) => JumlahKaloriCubit()),
        BlocProvider(create: (_) => RiwayatCubit()),
        BlocProvider(create: (_) => DetailRiwayatCubit()),
      ],
      child: MaterialApp(
        title: 'SkripGo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        debugShowCheckedModeBanner: false,
        home: SplashScreen(),
      ),
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    getPref();
  }

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var tokenAccess = preferences.getString("access_token");
    print(tokenAccess);
    if (tokenAccess != null) {
      await context.bloc<UserCubit>().getUser(tokenAccess);
      context.bloc<FamilyCubit>().getFamily();
      context.bloc<TotalCubit>().getTotal();
      context.bloc<CaloriesCubit>().getCalories();
      context.bloc<RiwayatCubit>().getRiwayat();
      return Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => MainPage()));
    } else {
      startSplashScreen();
    }
  }

  startSplashScreen() async {
    var duration = const Duration(seconds: 5);
    return Timer(duration, () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => SignInPage()),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    var boxDecoration = BoxDecoration(
      // Box decoration takes a gradient
      gradient: LinearGradient(
        // Where the linear gradient begins and ends
        begin: Alignment.topRight,
        end: Alignment.bottomLeft,
        // Add one stop for each color. Stops should increase from 0 to 1
        stops: [0.1, 2],
        colors: [
          Color(0xff1a237e),
          Color(0xff1a237e),
        ],
      ),
    );
    return Scaffold(
      body: Container(
        decoration: boxDecoration,
        child: Center(
            child: Container(
          child: Column(
            //
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/img/logosehat.png"))),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Text('SMART GROCERY',
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            letterSpacing: 3)),
                    SizedBox(height: 5),
                    Text('App For Family Safe and Healthy',
                        style: TextStyle(color: Colors.white)),
                  ],
                ),
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: Text("Version 1.1"),
                  ))
            ],
          ),
        )),
      ),
    );
  }
}
